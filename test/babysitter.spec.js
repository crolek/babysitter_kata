var assert = require("chai").assert;
var babySitter = require("../src/babysitter").babySitter;

describe("`BabySitter`", function() {
    describe("`parseTime`", function() {
        it("should take a 12 hour time and convert it to 24 hours", function(){
            assert.equal(babySitter.parseTimeTo24Hours("5 am"), 5);
            assert.equal(babySitter.parseTimeTo24Hours("5pm"), 17);
            assert.equal(babySitter.parseTimeTo24Hours("11:45pm"), 23);
            assert.equal(babySitter.parseTimeTo24Hours("12:01am"), 0);
        });
    });

    describe("`getTimeInsideRange`", function() {
        it("should return true or false", function() {
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.startToBedtime, 17, 20), 3)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.startToBedtime, 18, 19), 1)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.bedtimeToMidnight, 20, 24), 4)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.bedtimeToMidnight, 21, 24), 3)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.bedtimeToMidnight, 20, 2), 4)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.bedtimeToMidnight, 17, 2), 4)
            assert.equal(babySitter.getTimeInsideRange(babySitter.hourlyRates.bedtimeToMidnight, 17, 23), 3)
        })
    })

    describe("`getTimeBySection`", function() {
        it("should return how much time is in each section", function() {
            var startTime = 17;
            var endTime = 23;
            assert.deepEqual(babySitter.getTimeBySection(startTime, endTime), {
                startToBedtime: 3,
                bedtimeToMidnight: 3,
                midnightToEnd: 0
            })

            endTime = 4;
            assert.deepEqual(babySitter.getTimeBySection(startTime, endTime), {
                startToBedtime: 3,
                bedtimeToMidnight: 4,
                midnightToEnd: 4
            })
        })
    })

    describe("`calculatePay`", function() {
        it("should return the correct payment for all times", function() {
            var startTime = "5pm";
            var endTime = "11pm";
            //Start and before midnight
            assert.equal(babySitter.calculatePay(startTime, endTime),
                (3 * babySitter.hourlyRates.startToBedtime.rate) + (3 * babySitter.hourlyRates.bedtimeToMidnight.rate))
            //Start and to End
            endTime = "4am";
            assert.equal(babySitter.calculatePay(startTime, endTime),
                (3 * babySitter.hourlyRates.startToBedtime.rate) + (4 * babySitter.hourlyRates.bedtimeToMidnight.rate)
                + (4 * babySitter.hourlyRates.midnightToEnd.rate))
            endTime = "7pm";
            assert.equal(babySitter.calculatePay(startTime, endTime),
                (2 * babySitter.hourlyRates.startToBedtime.rate))
        })
    });
});