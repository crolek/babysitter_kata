module.exports = function(grunt) {
    grunt.loadNpmTasks("grunt-mocha-test");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-bump");

    // Project configuration.
    grunt.initConfig({
        bump: {
            options: {
                push: false
            }
        },
        mochaTest: {
            test: {
                options: {
                    reporter: "spec"

                },
                src: ["test/*.js"]
            }
        },
        watch: {
            prod: {
                files: ["src/*.js", "test/*.js"],
                tasks: ["mochaTest"]
            }
        }
    });

    grunt.registerTask("prod", ["mochaTest"]);
};