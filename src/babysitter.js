var moment = require("moment");

module.exports.babySitter = {
    bedTime: 20, //24 hour time
    hourlyRates: {
        startToBedtime: {
            rate: 12,
            start: 17,
            end: 20
        },
        bedtimeToMidnight: {
            rate: 8,
            start: 20,
            end: 24
        },
        midnightToEnd: {
            rate: 16,
            start: 24,
            end: 28
        }
    },
    parseTimeTo24Hours: function(time) {
        return parseInt(moment(time, ["h:mm A"]).format("H"), 10);
    },
    calculatePayPerSection: function(hours, rate) {
        return hours * rate;
    },
    resolveAfterHoursTime: function(time) {
        if (time <= 11) {
          return time + 24; //to help with the 1-4am times
        }
        return time;
    },
    //yikes, this needs refactored
    getTimeInsideRange: function(timeSection, startTime, endTime) {
        startTime = this.resolveAfterHoursTime(startTime);
        endTime = this.resolveAfterHoursTime(endTime);

        if (startTime < timeSection.start && endTime < timeSection.start) {
            return 0; //nothing in this timesection
        }

        if (startTime >= timeSection.start && endTime <= timeSection.end) {
            if (endTime < startTime) {
                return timeSection.end - startTime;
            }
            return endTime - startTime;
        }
        if (startTime >= timeSection.start && endTime >= timeSection.end) {
            return timeSection.end - startTime;
        }

        if (startTime < timeSection.start && endTime <= timeSection.end) {
            return endTime - timeSection.start;
        }

        if (startTime < timeSection.start && endTime > timeSection.end) {
            return timeSection.end - timeSection.start;
        }

        return false;
    },
    getTimeBySection: function(startTime, endTime) {
        return {
            startToBedtime: this.getTimeInsideRange(this.hourlyRates.startToBedtime, startTime, endTime),
            bedtimeToMidnight: this.getTimeInsideRange(this.hourlyRates.bedtimeToMidnight, startTime, endTime),
            midnightToEnd: this.getTimeInsideRange(this.hourlyRates.midnightToEnd, startTime, endTime)
        }
    },
    calculatePay: function(startTime, endTime){
        var total = 0;
        startTime = this.parseTimeTo24Hours(startTime)
        endTime = this.parseTimeTo24Hours(endTime)
        var hoursPerSection = this.getTimeBySection(startTime, endTime);
        console.log("Hours per section:: ", hoursPerSection)
        total += this.calculatePayPerSection(hoursPerSection.startToBedtime, this.hourlyRates.startToBedtime.rate);
        total += this.calculatePayPerSection(hoursPerSection.bedtimeToMidnight, this.hourlyRates.bedtimeToMidnight.rate);
        total += this.calculatePayPerSection(hoursPerSection.midnightToEnd, this.hourlyRates.midnightToEnd.rate);
        return total;
    }
}
