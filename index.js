var express = require("express");
var app = express();
//var env = require("dotenv").config();
var port = process.env.PORT || 10000; // ||  env.PORT;
var pkg = require("./package.json");
var version = pkg.version || "1.0.0";
var bodyParser = require("body-parser");
var babySitter = require("./src/babysitter").babySitter;
var assert = require("chai").assert;

//validating system vars are there before allowing the app to standup.
//this prevents a rollout without missing a newly created system var.
var systemVarKeys = ["EXAMPLE_VAR"];
systemVarKeys.forEach(function(varKey){
    assert.isDefined(process.env[varKey], "Missing a required system var");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get("/", function (req, res) {
    res.send("Hi Ben!");
});

app.get("/version", function(req, res) {
    res.send({
        version: version
    });
});

app.get("/calculate", function(req, res){
   res.send("You've got the right URL, but it's only a POST. Post something like { \"start\": \"5pm\", \"end\": \"9pm\" }")
});

app.post("/calculate", function (req, res) {
    var input = req.body;

    //need to make better validation
    if (input.start && input.end) {
        res.send({
            pay: babySitter.calculatePay(input.start, input.end)
        });
    } else {
        res.send({
            errorMessage: "Please make sure the Start and End time are in a 99am or 99pm form."
        });
    }

});

app.listen(port, function () {
    console.log("Example app listening on port: " + port);
});