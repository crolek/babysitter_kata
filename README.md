#Pillar DevOps and Dev Kata


# Getting Started:
Global Dependencies:
```
npm install mocha grunt-cli -g
```


App Dependencies:

`npm install`

To run:

`npm start`

For Unit Tests:

`mocha test` or `grunt mochaTest`


# ENV Vars
Since no secrets are included I could have included the .env file, but its bad practice.
Save the following files into a `.env` file in the project's root directory
```
PORT=10000
EXAMPLE_VAR=test
```

# To Use:
Curl at
Url: http://localhost:10000/calculate
Content-Type: application/json
Body: 
```
{
 "start": "5pm",
 "end": "9pm" 
}
```
It should return a response like:
```
{
    "pay": 44
}
```

# TODO
* Add Commander or something for a CLI
* Docker file :)
* Add input time validations